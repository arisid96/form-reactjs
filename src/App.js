import React from 'react';
import ValidateForm from './components/VlidateForm';
import './App.css';

function App() {
  return (
    <div className="App">
        <ValidateForm />
    </div>
  );
}

export default App;
