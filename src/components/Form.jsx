import React, {Component} from 'react';

class Form extends Component{
    state={
        name    : '',
        about   : '',
        hobby   : {
            coding: 'Coding',
            design: 'Design',
            other: 'Lainnya'
        },
        gender  : {
            male: 'Pria',
            female: 'Wanita'
        },
        job     : ''
    }

    handleField = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = event =>{
        event.preventDefault();
        console.log(this.state)
    }

    render(){
        return(
            <React.Fragment>
                <h1>Form Berkenalan Ama Si Dingko!</h1>
                <form onSubmit={this.submitHandler}>
                    <input 
                        value={this.state.name}
                        onChange={this.handleField}
                        name="name"  
                        placeholder="Your name"/>
                    <textarea 
                        value={this.state.about}
                        onChange={this.handleField}
                        name="about"
                        placeholder="Tell us about you" />
                    <label>Apa Hobby Kamu</label>
                    <label><input 
                        type="checkbox"
                        onChange={this.handleField}
                        value={this.state.hobby.coding} 
                        name="hobby"/> Coding</label>
                    <label><input 
                        type="checkbox"
                        onChange={this.handleField} 
                        value={this.state.hobby.design} 
                        name="hobby"/> Design</label>
                    <label><input 
                        type="checkbox"
                        onChange={this.handleField} 
                        value={this.state.hobby.other} 
                        name="hobby"/> Lainnya</label>
                    <label>Kamu seorang</label>
                    <label><input 
                        type="radio"
                        name="gender"
                        onChange={this.handleField} 
                        value={this.state.gender.male}/> Pria</label>
                    <label><input 
                        type="radio"
                        name="gender"
                        onChange={this.handleField} 
                        value={this.state.gender.female}/> Wanita</label>
                    <label>Apa profesimu</label>
                    <select 
                        onChange={this.handleField} 
                        value={this.state.job}
                        name="job">
                        <option>Programmer</option>
                        <option>Designer</option>
                        <option>Hacker</option>
                    </select>
                    <button type="submit">Daftar</button>
                </form>
            </React.Fragment>
        )
    }
}

export default Form;